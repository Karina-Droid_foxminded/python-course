def reverse_text(text):
    nums = []
    positions = []
    pos = -1
    for l in text:
        pos += 1
        try:
            int(l)
            nums.append(l)
            positions.append(pos)
            text = text.replace(l, '')
        except ValueError:
            pass

    mirrowed = text[::-1]
    for i in list(zip(nums, positions)):
        #i[0] is a number and i[1] - on which position it should be
        mirrowed = mirrowed[:i[1]] + i[0] + mirrowed[i[1]:]

    return mirrowed

if __name__ == '__main__':
    cases = [
        (':mdm2929ms1', ),
        ('sgg1 jan2', 'naj1 ggs2'),
        ('', ''),
    ]

    for text, reversed_text in cases:
        assert reverse_text(text) == reversed_text